# domilektor

## Site de Dominique Pagani

Produit par [lektor](https://www.getlektor.com/).

Le contenu rédactionnel est dans le dossier /content. Ses fichiers sont au format [textile](https://textile-lang.com/).

Bientôt la procédure pour les contributeurs.
