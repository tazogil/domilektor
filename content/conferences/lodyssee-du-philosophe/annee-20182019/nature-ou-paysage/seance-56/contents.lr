_model: page
---
title: Séance 5.6
---
etiquette: Le Paysage est une intériorisation de l’extérieur
---
body:

#### video ####
text:

h1. Le Paysage est une intériorisation de l’extérieur

h3(info). 5ème atelier, 6ème séance. Avril 2019

----
video: C4UjSXxfGBo

#### article ####
text:

h2. Incendie de Notre-Dame.

h3(tps#id0). Début

Les ogives de Notre-Dame divergent comme divergent les voix de cet art nouveau : le Novum Organum[1]

h3(tps#id6min35). 6 min 35

Vie, poésie et folie de Friedrich Hölderlin, de Wilhelm Waiblinger[2] (~1830) : relation entre Hölderlin tombé dans la folie et le menuisier qui l'accueille.

h3(tps#id12min). 12 min

Messe de Noël, Léonin (~1200) : nouveau mouvement, l'organum, apparition de la polyphonie "avec Léonin et Pérotin":https://www.youtube.com/watch?v=6mYfzf5O8QY [3]

h2. Intériorisation de l'extérieur (c'est-à-dire le paysage)

h3(tps#id19min35). 19 min 35

Élégies de Duino et Sonnets à Orphée, Rilke (1923, trad. par Armel Guerne[4]) : premier sonnet ("Alors un arbre s'éleva...") [5].

h3(tps#id29min35). 29 min 35

Aube[6], dans Illuminations, Rimbaud (1873)

h3(tps#id32min10). 32 min 10

Novalis, &laquo; Les hymnes à la nuit &raquo;[8] ( Grains de pollen), la nuit comme un écran sur lequel jaillit, comme dans un rêve, toute une fantasmagorie lumineuse.

!{width:120px}https://upload.wikimedia.org/wikipedia/commons/7/7e/Jeanpaul.jpg!

__Jean Paul__

Choix de rêves  de Jean Paul. [10] Le premier à avoir fait du rêve un thème littéraire.

Siebenkäs, Jean Paul[7] (1797).

Description du printemps, ici l'extérieur, le paysage, est decrit « en mouvement » avec des passages « cinématographiques ».

Cet extrait fait le récit d'un voyage du héros au début du printemps, cette marche à pied va inspirer Schumann pour sa première symphonie[9].

h3(tps#id1h01min30). 1 h 01 min 30

La grande forme, la symphonie :

* la forme FUGUE (Bach) un seul thème.
* la forme SONATE : conflit entre plusieurs thèmes, plus en phase avec la nouvelle sensibilité moderne du conflit intérieur.

**9e symphonie, Schubert (1825)**[10].

Grande forme SONATE, symphonie du voyage.

Marche : andante du 2ème mouvement.

Alternance entre l'intériorité et l'extérieur du panorama.

"Ouverture des portes du paradis", **intérieur** (comme dans ce qui se passe avec le voyageur de Jean Paul).

Impossible de s'arracher à ce paradis, la Grande Forme est suspendue, comme l'a écrit Claude Levy Strauss dans l'introduction au &laquo; Cru et le Cuit &raquo; [11] :

bq. Tout ce passe comme si la musique entrait dans le temps pour mieux en sortir.


<hr>

fn1. "Chant Gréogorien. Novum Organum":https://fr.wikipedia.org/wiki/Organum

fn2. "Wilhelm Waiblinger. Ami de Hölderlin":https://en.wikipedia.org/wiki/Wilhelm_Waiblinger

fn3. "Ensemble Organum. un groupe de musique ancienne, spécialisé dans l'interprétation de la musique vocale médiévale. ":https://fr.wikipedia.org/wiki/Ensemble_Organum

fn4. "Armel Guerne. Résistant, traducteur de Rilke":https://fr.wikipedia.org/wiki/Armel_Guerne

fn5. "Rilke. Les Sonnets à Orphée":https://fr.wikipedia.org/wiki/Sonnets_%C3%A0_Orph%C3%A9e#Les_sonnets

fn6. "Rimbaud. Aube dans Illuminations":https://www.poesie-francaise.fr/arthur-rimbaud/poeme-aube.php

fn7. "Jean Paul. Siebenkäs":https://www.persee.fr/doc/roman_0048-8593_1978_num_8_20_5177

fn10. "Jean Paul. Choix de rêves":https://www.jose-corti.fr/titres/choix-de-reves.html

fn8. "Novalis. Les hymnes à la nuit":https://fr.wikisource.org/wiki/Hymnes_%C3%A0_la_nuit

fn9. "Schumann. Première Symphony, le Printemps - Karajan":https://www.youtube.com/watch?v=FuBDGAHBedo

fn10. "Schubert. 9e symphonie (1825)":https://www.youtube.com/watch?v=QjdCHESuROs

fn11. "Levy-Strauss. Le Cru et le Cuit":https://fr.wikipedia.org/wiki/Le_Cru_et_le_Cuit

----
startcell:
----
endcell:
----
positionnement: normal
----
decoration: normal
---
ordre: 3
