# -*- coding: utf-8 -*-
from lektor.pluginsystem import Plugin


class GridAreasPlugin(Plugin):
    name = 'Grid areas'
    description = u'Add your description here.'

    def on_setup_env(self, **extra):
        self.env.jinja_env.globals.update(
            get_areas=self.get_areas
        )

    def get_areas(self, page):
        blocks = page._data["body"]._blocks
        self.cols_number = int(page._data["colonnes"][4])
        cells = [(b["startcell"], b["endcell"]) for b in blocks]
        self.create_letters_range(cells)
        self.create_base_grid()
        self.fill_base_grid(cells)
        return self.create_areas()

    def create_areas(self):
        retval = []
        for letter in self.letters_range:
            r = ["" for i in range(self.cols_number + 1)]
            for (k, v) in self.base_grid.items():
                ln = k[0]
                n = int(k[1]) - 1
                if letter == ln:
                    x = "." if v is None else v
                    r[n] = x
            retval.append(" ".join(r).strip())
        return '"{}"'.format('" "'.join(retval))

    def create_letters_range(self, cells):
        """
        set self.letters_range, a list from the first letter ('A') and the
        last one among the cells names.

        """
        letters = []
        for (startcell, endcell) in cells:
            letters.append(startcell[0])
            letters.append(endcell[0])
        unduplicated_letters = set(letters)
        self.letters_range = sorted(list(unduplicated_letters))

    def create_base_grid(self):
        """
        set self.base_grid, a dict with as keys, the concatenation of each
        letters in self.letters_range and the range from 1 to the number of
        columns and None as values.

        """
        self.base_grid = dict()
        for letter in self.letters_range:
            for i in range(1, self.cols_number + 1):
                s = letter + str(i)
                self.base_grid[s] = None

    def fill_base_grid(self, cells):
        """
        Set the value of a key of self.base_grid keys with a cell name.


        """

        for (startcell, endcell) in cells:
            cell = "{}{}".format(startcell, endcell)
            letter1 = startcell[0]
            letter2 = endcell[0]
            col1 = int(startcell[1])
            col2 = int(endcell[1])
            concat1 = ["{}{}".format(letter1, i) for i in range(col1, col2+1)]
            concat2 = ["{}{}".format(letter2, i) for i in range(col1, col2+1)]
            for c in concat1:
                self.base_grid[c] = cell
            for c in concat2:
                self.base_grid[c] = cell
