
/*
Pilotage d'une vidéo depuis les liens
dans la page.
Voir templates/blocks/video.html
*/
// l'instance du player
var player;
/*
si un paramètre d'URL est fourni, le passer
à goTOVideo pour faire des liens en page d'Accueil
qui envoient à des moments dans la page spécifiée.
*/
function onPlayerReady(event){
  var p = window.location.search;
  if(p != ""){
    goToVideo(p.substr(1));
  }
}
function stopVideo() {
  player.stopVideo();
}
/*déplace la vidéo au temps donné par 'timing'*/
function goToVideo(timing){
  var seconds = 0, h = 1, m = 1, s = 0;
  var timing = timing.substr(2);
  var ph = timing.indexOf('h');
  if(ph > -1){
    h = timing.substring(0, ph);
    h = parseInt(h);
    timing = timing.substr(ph+1);
   seconds = 60 * 60 * h;
  }
  var pm = timing.indexOf('min');
  if(pm > -1){
    m = timing.substring(0, pm);
    m = parseInt(m);
    timing = timing.substr(pm+3);
    seconds += 60 * m;
  }
  s = parseInt(timing);
  if(Number.isInteger(s)){
    seconds += s;
  }
  player.playVideo();
  player.seekTo(seconds, true);
  var pv = document.getElementById("player-video");
   window.scrollTo(0, pv.offsetTop);
}
$(function(){
  /*les titre h3.tps callent la vidéo au templates
  donné par leur attribut ID.
  */
  $(".tps").each(function( index ){
    $(this).on('click', function(e){
      // voir la fonction ci-dessus
      goToVideo($(this).attr("id"));
    });
  });
});
