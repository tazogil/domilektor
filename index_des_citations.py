# -*- coding: utf-8 -*-
from glob import glob
from os.path import join, dirname, abspath
import re
from math import ceil


class IndexDesCitationsPlugin():
    name = 'Index des citations'
    description = u'Production de /references contenant les références cités '
    'dans les notes de bas de page .'
    index = dict()
    output_file = "references/contents.lr"
    sep = "body:"

    def __init__(self):
        self.root = join(dirname(abspath(__file__)), 'content')
        files = glob("{}/**/contents.lr".format(self.root), recursive=True)
        for f in files:
            if self.output_file not in f:
                data = self.extract_notes(f)
                self.create_index(data)

        self.save_index()

    def extract_notes(self, f):
        """
        Construit :
            self.lignes une liste fait des notes de bas de page
            self.notes la même liste mais d'un tuple (note, auteur, titre)
            self.url l'url de la page courante
            self.index_titres les lignes commençant par h3
            self.titre le titre de la page courante

        """
        retval = dict()
        with open(f) as target:
            lcr = len("contents.lr")
            lr = len(self.root)
            retval["url"] = f[lr:][:-lcr]
            retval["lignes"] = [l for l in target.readlines()]
            retval["notes"] = []
        for l in retval["lignes"]:
            if l[:2] == "fn":
                parts = l.split('.')
                try:
                    note = parts[0][2:]
                    auteur = parts[1].replace('"', '').strip().capitalize()
                    if '(' in auteur:
                        p = auteur.index('(')
                        auteur = auteur[:p].strip()
                    titre = parts[2]
                    tparts = titre.split(':')
                    titre = tparts[0].replace('"', '').strip()
                except IndexError:
                    pass
                else:
                    retval["notes"].append((note, auteur, titre))
            elif l[:5] == 'title':
                retval["title"] = l[6:].strip()

        retval["index_titres"] = [i for (i, l)
                                  in enumerate(retval["lignes"])
                                  if l[:6] == "h3(tps"]
        return retval

    def create_index(self, data):
        """
        Contruit self.index un dict : auteur: (
                                        titre de la note
                                        titre de la page
                                        url de la page
                                        id du h3 se référence à la note
                                        titre du h3 se référence à la note
                                        )

        """
        for (note, auteur, titre) in data["notes"]:
            xnote, xligne = 0, 0
            for (i, ligne) in enumerate(data["lignes"]):
                idx = "[{}]".format(note)
                if idx in ligne:
                    xnote = i
                    break
            for it in data["index_titres"]:
                if it < xnote:
                    xligne = it
            if auteur not in self.index:
                self.index[auteur] = []
            if xligne > 0:
                ligne_h3 = data["lignes"][xligne]
                res = re.match("^h3\(tps(#id.+)\)", ligne_h3)
                if res:
                    tpsid = res.group(1)
                    if not (titre, tpsid) in [(d[0], d[3])
                                              for d in self.index[auteur]]:
                        self.index[auteur].append((titre,
                                                   data["title"],
                                                   data["url"],
                                                   tpsid))
                else:
                    print(ligne_h3)

    def save_index(self):
        """
        Ecrit self.index dans content/references/contents.lr

        """
        output_file_path = join(self.root, self.output_file)
        output_text = ""
        with open(output_file_path) as fh:
            t0 = fh.read()
            (t1, t2) = t0.split(self.sep)
            ts = (t1, 'body:', "\n", '#### article ####',
                  "\n", "text:", "\n",
                  "h1. Index des auteurs et des références", "\n")
            output_text = "\n".join(ts)

        lignes = []
        l = len(self.index)
        n = ceil(l / 2)

        for i, (k, v) in enumerate(sorted(self.index.items()), 1):
            lignes.append("- {} :=".format(k.upper()))
            for d in v:
                lignes.append('* {} {} "{}":{}{}'.format(i, d[0], d[1], d[2], d[3]))
            if i % n == 0:
                lignes.append("\n")
                lignes.append('#### article ####')
                lignes.append('text:')
        retval = "\n".join(lignes)
        output_text += retval
        with open(output_file_path, "w") as fh:
            fh.write(output_text)


IndexDesCitationsPlugin()
